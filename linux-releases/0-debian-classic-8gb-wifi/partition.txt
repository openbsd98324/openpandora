


## CUSTOM

Disk /dev/mmcblk0: 15.7 GB, 15719727104 bytes
246 heads, 62 sectors/track, 2013 cylinders
Units = cylinders of 15252 * 512 = 7809024 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000

        Device Boot      Start         End      Blocks   Id  System
/dev/mmcblk0p1               1          68      518537   83  Linux
/dev/mmcblk0p2              69        2013    14832570   83  Linux
