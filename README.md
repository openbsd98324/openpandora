 
 The ARM Cortex-A8 is a 32-bit processor core licensed by ARM Holdings implementing the ARMv7-A architecture.


 The ARM EABI port (armel) is the default port in Debian for ARM architecture versions 4T, 5T, and 6.

From Debian 10 (buster), 4T is no longer supported and the armel baseline was updated to 5T. If you have 4T hardware, please use Debian stretch or upgrade your hardware.

Other ports to ARM hardware exist / have existed in Debian - see ArmPorts for more links and an overview


 
 
 # Linux/BSD for openpandora







## Content 

Mirror of openpandora and more

![](image/pandora-minipc.png)


" The pandora has been opened ..."



## Machine and hardware


````

The new 1GHz ARM Cortex brings the Pandora up to date and almost doubles its processing power. Unlike tablets and smartphones, you can use this power in the full linux desktop environment.

Now with 512MB RAM, the Pandora has plenty of memory for more demanding applications and more multitasking.

The NEON co-processor and SGX graphics chip are instrumental to top-notch emulator and 3D gaming performance. The open nature of the Pandora 1GHz lets developers get right down to the metal, squeezing out every iota of power.

Specifications
1GHz (+ overclock support) ARM Cortex A8
PowerVR SGX GPU with OpenGL ES 2.0 support
512MB DDR SDRAM RAM and 512MB internal NAND storage
4.3-inch 800x480 touchscreen TFT-LCD
Stereo speakers, analogue volume wheel, microphone, high-quality DAC
QWERTY keyboard, gaming controls, dual analogue
802.11b/g Wi-Fi, Bluetooth 2.0 + EDR
USB On-The-Go + full-sized USB 2.0 host port
Two SDHC/SDXC compatible SD slots
4200mAh lithium-ion polymer battery, 10 hour battery life

````



## Default OS (Zaxxon)





![](medias/1679345090-screenshot.png)

![](medias/1679345063-screenshot.png)

http://openpandora.org/firmware/fullflash/SuperZaxxon161.zip

1. Download the ZIP archive and extract it to the root directory of your SD Card.

2. Insert the SD card in the LEFT card slot, keep the right shoulder button pressed while switching on the Pandora and choose to boot 
from SD Card.

3. Follow the instructions on screen.

4. When booting for the first time, be sure to wait until the First Boot Wizard appears.


https://pandorawiki.org/%C3%85ngstr%C3%B6m


## Zaxxon 1.55

Image ready to use.


 http://qemu.openpandora.org/PandoraOperatingSystems/Zaxxon1.55-31.07.2013.imz

 Older, but working. 
 SSL might be non working.

Working:
 links -g netbsd.org 

Firefox is installed.


## Linux OpenPandora 6.0 Debian armel 

````
user@pandora:~$ uname -a
Linux pandora 3.2.61 #1017 Sat Jul 12 01:57:35 EEST 2014 armv7l GNU/Linux

lsmod :

Module                  Size  Used by
ipv6                  232265  10 
aes_generic            33453  2 
wl1251_sdio             3319  0 
wl1251                 69101  1 wl1251_sdio
mac80211              223861  2 wl1251_sdio,wl1251
cfg80211              154737  2 wl1251,mac80211
ads7846                 8481  0 
hwmon                    964  1 ads7846

````


![](medias/panscreen.png)


## Linux Kernel

Default Super Zaxxon runs:

SuperZaxxon 1.61 

Kernel : openpandora 3.2.57 #982 Sun Apr 20 20:33:11 EEST 2014 armv7l GNU/Linux
Booting with : 

````
cat /proc/cmdline 
ubi.mtd=4 ubi.mtd=3 root=ubi0:rootfs rootfstype=ubifs rw rootflags=bulk_read vram=6272K omapfb.vram=0:3000K mmc_core.removable=0 quiet
````


```` 
rootfs on / type rootfs (rw)
ubi0:rootfs on / type ubifs (rw,relatime,bulk_read)
devtmpfs on /dev type devtmpfs (rw,relatime,size=91944k,nr_inodes=22986,mode=755)
proc on /proc type proc (rw,relatime)
tmpfs on /mnt/.splash type tmpfs (rw,relatime,size=40k)
sysfs on /sys type sysfs (rw,relatime)
none on /dev type tmpfs (rw,relatime,mode=755)
devpts on /dev/pts type devpts (rw,relatime,gid=5,mode=620)
usbfs on /proc/bus/usb type usbfs (rw,relatime)
tmpfs on /var/volatile type tmpfs (rw,relatime)
tmpfs on /dev/shm type tmpfs (rw,relatime,mode=777)
tmpfs on /media/ram type tmpfs (rw,relatime)
ubi1:boot on /boot type ubifs (rw,relatime)
````



````
openpandora:/boot$ ls
autoboot.txt  bootmenu.txt  uImage  uImage-2.6.27.57-omap1  uImage-3

/boot$ md5sum *
42b3a5f7bdd88da13fb441c61cb22164  autoboot.txt
9afae78f8b2689990feca3e3d0680969  bootmenu.txt
7b72d1fe36fa21b56f66472d332e4210  uImage
37073081713fc2de2c1d62cc94829f02  uImage-2.6.27.57-omap1
7b72d1fe36fa21b56f66472d332e4210  uImage-3

B^@Linux-3.2.57^@ <---  uImage-3
````



````  
with WIFI:
/boot$ lsmod
Module                  Size  Used by
aes_generic            33453  2 
bluetooth             136446  0 
wl1251_sdio             3319  0 
wl1251                 69101  1 wl1251_sdio
mac80211              223853  2 wl1251_sdio,wl1251
cfg80211              154737  2 wl1251,mac80211
ecb                     1616  0 
g_cdc                  31886  0 
omaplfb                 6407  0 
pvrsrvkm              142886  1 omaplfb
zram                   10294  1 
snd_seq                47265  0 
snd_seq_device          4491  1 snd_seq
snd_pcm_oss            37889  0 
snd_mixer_oss          14120  1 snd_pcm_oss
ipv6                  232081  10 
fuse                   58183  2 
ads7846                 8481  0 
hwmon                    964  1 ads7846
````



````
wl1251

./etc/init.d/wl1251-init
./etc/rc0.d/K40wl1251-init
./etc/rc1.d/K40wl1251-init
./etc/rc2.d/S30wl1251-init
./etc/rc3.d/S30wl1251-init
./etc/rc4.d/S30wl1251-init
./etc/rc5.d/S30wl1251-init
./etc/rc6.d/K40wl1251-init
./lib/firmware/wl1251-fw.bin
./lib/modules/2.6.27.57-omap1/updates/kernel/drivers/net/wireless/wl12xx/wl1251_sdio.ko
./lib/modules/2.6.27.57-omap1/updates/kernel/drivers/net/wireless/wl12xx/wl1251.ko
./lib/modules/3.2.57/kernel/drivers/net/wireless/wl1251
./lib/modules/3.2.57/kernel/drivers/net/wireless/wl1251/wl1251_sdio.ko
./lib/modules/3.2.57/kernel/drivers/net/wireless/wl1251/wl1251.ko
./sys/bus/sdio/drivers/wl1251_sdio
./sys/module/cfg80211/holders/wl1251
./sys/module/mac80211/holders/wl1251
./sys/module/mac80211/holders/wl1251_sdio
./sys/module/wl1251
./sys/module/wl1251/holders/wl1251_sdio
./sys/module/wl1251_sdio
./usr/lib/opkg/info/wl1251-modules.postrm
./usr/lib/opkg/info/wl1251-modules.list
./usr/lib/opkg/info/wl1251-modules.prerm
./usr/lib/opkg/info/wl1251-modules.postinst
./usr/lib/opkg/info/wl1251-modules.control
./proc/irq/181/wl1251
./media/retropie/usr/src/linux-headers-4.14.98-v7+/drivers/net/wireless/ti/wl1251
./media/retropie/usr/src/linux-headers-4.14.98+/drivers/net/wireless/ti/wl1251
````


how to mount a nand:
````
   mount /dev/nand       /media/nand  
````








## Linux Kernel

````
2.6.27.57-omap1
3.2.30
3.2.45
3.2.57
3.2.61
````

## Classic Armel 


https://gitlab.com/openbsd98324/openpandora/-/tree/main/classic-armel-wifi-default-v1

ext2 and ext3:
````
cp -a mmcblk0p1/*  /media/sda1/   ;   cp -a mmcblk0p2/*   /media/sda2/  ; sync 
````

https://gitlab.com/openbsd98324/openpandora/-/raw/main/linux-releases/01.pandora-armel-wifi/media.tar.gz


## Wifi Armel 


wl1251 

dhclient 


## 1. Current lite

https://gitlab.com/openbsd98324/openpandora/-/tree/main/linux-releases


CURRENT - Stable Release with wifi: 

https://gitlab.com/openbsd98324/openpandora/-/tree/main/linux-releases/02.debian-squeeze-wifi-armv7l




## 2. Classic Mark with full Desktop (default)


The classic desktop openpandora, mark with xfce and the kernel, the mbr is included.

"https://gitlab.com/openbsd98324/openpandora/-/raw/main/linux/openpandora-desktop-mark/mark-firmware-openpandora-armhf.tar.gz

"https://gitlab.com/openbsd98324/openpandora/-/raw/main/linux/openpandora-desktop-mark/pandian-mark2-hf_2014-01-02.img.gz"


(debian, systemd, armhf, kernel: https://gitlab.com/openbsd98324/openpandora/-/blob/main/linux/pandian/v3/mark/uImage-3.2.45-hf)




## 3. Debian stretch, ARMHF, single partition of 8gb (ext2):

https://gitlab.com/openbsd98324/openpandora/-/tree/main/linux/debian-stretch-base-x11
  (see info.txt)
   
  8gb image SD/MMC with ext2/ext3 partitions.




## 4. Debian, Image with armel, with wifi (two partitions)

wget -c https://gitlab.com/openbsd98324/openpandora/-/raw/main/linux-releases/01.pandora-armel-wifi/image-pandora-sd-mmc-left-classic-armel-armv7l-debian-ready-v1.img.gz

Copy on SD/MMC card, and start the pandora with the SD (left slot):

zcat image-pandora-sd-mmc-left-classic-armel-armv7l-debian-ready-v1.img.gz > /dev/sdX

  wpa_supplicant  -i wlan0 -c /etc/wpa_supplicant.conf 

  (wait few secs)

  dhclient -v wlan0

  apt-get update 

  apt-get install mpg123 







## 5 WIFI ON SQUEEZE


deb http://archive.debian.org/debian squeeze main

Linux pandora 3.2.61 #1017 Sat Jul 12 01:57:35 EEST 2014 armv7l GNU/Linux

````
/media/mmcblk0p1/autoboot.txt 
 setenv bootargs root=/dev/mmcblk0p2 rw rootwait vram=6272K omapfb.vram=0:3000K mmc_core.removable=0 video=/dev/fb0
 ext2load mmc 0:1 0x80300000 /boot/uImage-3
 bootm 0x80300000
````


````
Module                  Size  Used by
aes_generic            33453  3 
ipv6                  232265  10 
fuse                   58183  1 
wl1251_sdio             3319  0 
wl1251                 69101  1 wl1251_sdio
mac80211              223861  2 wl1251_sdio,wl1251
cfg80211              154737  2 wl1251,mac80211
ads7846                 8481  0 
hwmon                    964  1 ads7846
````

  DMESG 

PAN #1:
````  
[    0.000000] Linux version 3.2.61 (notaz@pixelinis) (gcc version 4.5.2 (Sourcery G++ Lite 2011.03-41) ) #1017 Sat Jul 12 01:57:35 EEST 2014
[    0.000000] CPU: ARMv7 Processor [411fc082] revision 2 (ARMv7), cr=10c5387d
[    0.000000] CPU: PIPT / VIPT nonaliasing data cache, VIPT nonaliasing instruction cache
[    0.000000] Machine: Pandora Handheld Console
[    0.000000] cma: CMA: reserved 4 MiB at 86000000
[    0.000000] Initialized persistent memory from 80fe0000-80ffffff
[    0.000000] cma: CMA: reserved 64 MiB at 8c000000
[    0.000000] Memory policy: ECC disabled, Data cache writeback
[    0.000000] On node 0 totalpages: 65536
[    0.000000] free_area_init_node: node 0, pgdat c05bb80c, node_mem_map c05ee000
[    0.000000]   Normal zone: 512 pages used for memmap
[    0.000000]   Normal zone: 0 pages reserved
[    0.000000]   Normal zone: 65024 pages, LIFO batch:15
[    0.000000] OMAP3430/3530 ES2.1 (l2cache iva sgx neon isp )
[    0.000000] Clocking rate (Crystal/Core/MPU): 26.0/332/500 MHz
[    0.000000] pcpu-alloc: s0 r0 d32768 u32768 alloc=1*32768
[    0.000000] pcpu-alloc: [0] 0 
[    0.000000] Built 1 zonelists in Zone order, mobility grouping on.  Total pages: 65024
[    0.000000] Kernel command line: root=/dev/mmcblk0p2 rw rootwait vram=6272K omapfb.vram=0:3000K mmc_core.removable=0 video=/dev/fb0
[    0.000000] PID hash table entries: 1024 (order: 0, 4096 bytes)
[    0.000000] Dentry cache hash table entries: 32768 (order: 5, 131072 bytes)
[    0.000000] Inode-cache hash table entries: 16384 (order: 4, 65536 bytes)
[    0.000000] Memory: 256MB = 256MB total
[    0.000000] Memory: 183888k/183888k available, 78256k reserved, 0K highmem
[    0.000000] Virtual kernel memory layout:
[    0.000000]     vector  : 0xffff0000 - 0xffff1000   (   4 kB)
[    0.000000]     fixmap  : 0xfff00000 - 0xfffe0000   ( 896 kB)

````  



PAN #2:
````  
````  

## Pyra Device... 


Newer Generation

````

1,5GHz DualCore ARM CPU, upgradable

A 6000mAh battery means you have a companion that will be there for you, through use and at times other devices have given up long ago. 
````


## RetroGaming (experimental)


NES, SMS, Atari, Amiga,...

![](image/retrogaming-arkanoid.png)


Experimental (no wifi):

https://gitlab.com/openbsd98324/openpandora/-/raw/main/linux/panretropie/v1/disk.mbr

https://gitlab.com/openbsd98324/openpandora/-/raw/main/linux/panretropie/v1/disk1.tar.gz

https://gitlab.com/openbsd98324/openpandora/-/raw/main/linux/panretropie/v1/mmcblk0p2.tar.gz




## BOOT 


````
Set up your boot.txt
Create a new file called boot.txt (or autoboot.txt if you want to boot automatically) and copy and paste the text below. Copy that file to the root of the root of the SD card.

If you edit the file on Windows, use an advanced text editor like Notepad++ and be sure to convert to UNIX format (in NP++: Edit -> EOL Conversion -> UNIX format). If you have DOS linebreaks, ext2load will fail with an error like "file not found" as it appends an hidden character to the uImage file name.

boot.txt (from the official firmware site)

setenv bootargs root=/dev/mmcblk0p1 rw rootwait vram=6272K omapfb.vram=0:3000K mmc_core.removable=0
ext2load mmc 0 0x80300000 /boot/uImage
bootm 0x80300000
Note: If you chose ext3 instead of ext2 for the partition containing the kernel, the second line still starts with ext2load. There is no ext3load.

Note: depending on firmware version kernels might be at several, sometimes multiple locations, so you have to choose one:

/boot/uImage-3 - the default 3.2 kernel (recommended)
/lib/boot/uImage - alternative location of 3.2 kernel on certain older firmwares
/boot/uImage - mostly a symlink to the latest kernel on recent firmwares
Be aware though that this boot.txt assumes you have formatted your card with ext2 and loads the kernel off the SD card. It is technically possible to boot the kernel from NAND but still run the rest of the file system from your SD card with following boot.txt

setenv bootargs root=/dev/mmcblk0p1 rw rootwait vram=6272K omapfb.vram=0:3000K mmc_core.removable=0
ubi part boot && ubifsmount boot && ubifsload ${loadaddr} uImage && bootm ${loadaddr} && boot
However this is not recommended because kernel modules on SD card will likely be not compatible with kernel on NAND.

Both boot.txt's assume you are booting from the left SD card slot, first partition. You can change "mmcblk0p1" to "mmcblk1p1" if you want to boot from the right slot (but boot.txt must still be on a card on the left slot).

Rescue console boot
If you have a problem booting, and need to rescue data from the NAND) you can boot into a console by doing the following: Make a file named boot.txt in root of SD with this[1]:

setenv bootargs ubi.mtd=4 ubi.mtd=3 root=ubi0:rootfs rootfstype=ubifs rw rootflags=bulk_read console=tty0 vram=6272K omapfb.vram=0:3000K init=/bin/bash
ubi part boot && ubifsmount boot && ubifsload 0x80300000 uImage && bootm 0x80300000 && boot
then hold down the right shoulder button on boot and booting from the SD card. (It doesn't require an operating system on the SD card, but it will use the boot configuration specified there). This is a text mode boot to the shell, using the kernel from the NAND. This should provide a way of bypassing any broken startup scripts, adding in logging for a normal boot, etc. It does not start the full OS, just a basic shell.


Setting up multiple-partition SD cards for booting
It is possible to have several partitions on the SD card and boot from one of them. E.g. if you have three partitions on the card like this:

Partition 1: FAT
Partition 2: ext2 (where the rootfs should be placed)
Partition 3: swap

How it's done:

1. Put uBoot's boot control files "boot.txt" and/or "autoboot.txt" into the root of the first partition of the card (FAT partition in this example)

2. Make "boot.txt" and "autoboot.txt" point U-Boot to the partition, which holds the root file system of your Linux system. This would be parition 2 in this example (ext2 FS). This is done using the "root" parameter of setenv.

3. Make "boot.txt" and "autoboot.txt" point U-Boot to the correct location to boot your kernel from. This kernel location can be any FAT or ext2/3/4 partition on the SD card. The uBoot commands "fatload" and "ext2load" with their parameter "mmc x:y" are repsonsible for loading the kernel. Choose the command, which addresses the file system the kernel is located on and make sure to correctly adapt the values x and y.

This common example boots the kernel from the FAT partition (then this is a "boot partition"), and uses the ext2 file system as root FS:

setenv bootargs root=/dev/mmcblk0p2 rw rootwait vram=6272K omapfb.vram=0:3000K mmc_core.removable=0
fatload mmc 0:1 0x80300000 uimage
bootm 0x80300000
If you put the kernel into the root file system's /boot directory, the second line would be different:

setenv bootargs root=/dev/mmcblk0p2 rw rootwait vram=6272K omapfb.vram=0:3000K mmc_core.removable=0
ext2load mmc 0:2 0x80300000 /boot/uImage
bootm 0x80300000
Note:

For both the "root=" kernel argument and the "mmc x:y" argument of the fatload/ext2load commands, slot numbering begins at 0 (0 is left SD slot, 1 is right SD slot) and partition numbering begins at 1!

Recommendation:

Put the kernel into another partition than the root FS is located. Reason: In case the root FS partition is flagged "inconsistent" after a system crash or sudden SD card removal, uBoot won't be able to boot from that partition anymore. But the file system cleanup routines can only be run, once the kernel has been booted. Hence it's safer to put the kernel on a different partition.

If you want to use ext4 for the root FS, it is necessary to put the kernel on a different partition.

Options Summary

Files	File System Options	Location
boot.txt/autoboot.txt	FAT, ext2, ext3	first partition of left SD card
kernel	FAT, ext2, ext3	parameters of fatload or ext2load
rootfs	ext2, ext3, ext4	"root" parameter of setenv
Boot the system
As you power up the Pandora, hold the shoulder button R. A menu should appear, allowing you to boot from the SD card. (this step isn't necessary if you chose to create an autoboot.txt instead of boot.txt). Remember that this will be an un-configured image, taking a little while longer to boot, and giving the first-run dialogue.

Access the NAND
Once you're booted into the system from SD, you may want access to the NAND rootfs. The following will let you do that.

sudo mkdir /mnt/nand
sudo ubiattach /dev/ubi_ctrl -m 4
sudo mount -t ubifs ubi0:rootfs /mnt/nand
Extending intall environment
An alternative approach to using an SD card to increase the space accessible to the system is OS Extend. This allows the root filesystem to exist on more than one physical device.


````

# Some specs 

````
Manufacturer	Open Pandora
Model name	Pandora
CPU	ARM Cortex A8
CPU speed	600 Mhz
Graphics	PowerVR SGX specs
OS	Linux
Display Size
4.3" 800 X 480
Display Type
Soft (Finger) Touch
RAM	512 MB
Weight (Minimum)	335gm / 0.74 pounds
Size	140/83/28.0 mm
Size	5.5/3.3/1.1 inches
Physical Interfaces	Multi-format card reader
Video-out (analogue)
Line-out / Headphone (3.5mm)
USB 2.0
Wireless Interfaces	802.11b/g
BT2.0
````

## References

[1.] https://dragonbox.de/en/

[2.] https://pyra-handheld.com/boards/


![](image/matrix-gif-image.gif)



# Customization 

![](medias/OpenPandora-machine-retropie.jpg)


![](medias/feh_025109_000001_20230821_221344.jpg)

![](medias/feh_025220_000001_20230821_222503.jpg)


