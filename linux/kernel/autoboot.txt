
##### Envoirenments
setenv bootargs root=/dev/mmcblk0p2 rw rootwait console=ttyS0,115200n8 vram=6272K omapfb.vram=0:3000K mmc_core.removable=0 video=/dev/fb0 

##### Load uImage and boot
#fatload mmc 0 ${loadaddr} uImage-3.2.45-hf
#fatload mmc 0 ${loadaddr} uImage-2.6.27.57-omap1
ext2load mmc 0:2 0x80300000 /usr/INSTALL/uImage-2.6.27.57-omap1
#boot/uImage-2.6.27.46-omap1
#bootm ${loadaddr}
bootm 0x80300000



# Some special
# setenv bootargs root=/dev/mmcblk0p1 rw rootwait vram=6272K omapfb.vram=0:3000K mmc_core.removable=0 debug psplash=false single

# Experimental Kernel
# ext2load mmc 0 ${loadaddr} /boot/uImage
#fatload mmc 0 ${loadaddr} uImage-3.2.39-hf


# Original Kernel
# ext2load mmc 0 ${loadaddr} /boot/uImage

# This is for second partition
#ext2load mmc 0:2 0x80300000 /boot/uImage

# Or
#setenv bootargs debug root=/dev/mmcblk0p1 rw rootdelay=2 console=ttyS0,115200n8 vram=6272K omapfb.vram=0:3000K
#ext2load mmc 0 0x80300000 /boot/uImage-2.6.27.46-omap1
#bootm 0x80300000

